package com.nesterov;

import java.security.*;

public class Main {

    public static void main(String[] args) {
        try {
            // Создаем объект для генерации ключей по алгоритму RSA.
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            // Устанавливаем размер ключа в битах.
            keyGen.initialize(2048);
            // Генерируем пару ключей.
            KeyPair keyPair = keyGen.generateKeyPair();
            // Получаем открытый ключ.
            PublicKey publicKey = keyPair.getPublic();
            // Получаем закрытый ключ.
            PrivateKey privateKey = keyPair.getPrivate();
            // Выводим сгенерированные ключи в консоль.
            System.out.println("Сгенерированный открытый ключ: " + publicKey);
            System.out.println("Сгенерированный закрытый ключ: " + privateKey);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}

